# Reployee Expense Reimbursement System (ERS) - Java

## Project Description

The Reployee Expense Reimbursement System (ERS) manages the process of reimbursing employees for expenses incurred while on company time. All employees in the company can login and submit requests for reimbursement and view their past tickets and pending requests. Finance managers can log in and view all reimbursement requests and past history for all employees in the company. Finance managers are authorized to approve and deny requests for expense reimbursement.

## Technologies Used

* Java 1.8
* Servlets
* JDBC
* SQL
* HTML
* CSS
* Javascript
* Bootstrap
* AJAX
* JUnit
* Log4j

## Environment

* Apache Tomcat v9.0 Server
* Oracle Database
* Eclipse/Spring Tools Suite 4
* DBeaver

## Features
Implemented User Stories:
* An Employee can login
* An Employee can view the Employee Homepage
* An Employee can logout
* An Employee can submit a reimbursement request
* An Employee can view their pending reimbursement requests
* An Employee can view their resolved reimbursement requests
* A Manager can login
* A Manager can view the Manager Homepage
* A Manager can logout
* A Manager can approve/deny pending reimbursement requests
* A Manager can view all pending requests from all employees
* A Manager can view all resolved requests from all employees and see which manager resolved it
* Unit testing of business logic/data validation using JUnit
* Logging of significant runtime events using Log4j

To-do List:
* An Employee can view their information (profile)
* An Employee can update their information
* An Employee can upload an image of his/her receipt as part of the reimbursement request
* A Manager can view all Employees
